# Materialien

Dieses Repository beinhaltet hilfreiches Material zur Open CoDE Plattform:

## Feature Liste
Die Feature List der Plattform Open CoDE bietet eine Übersicht über die wichtigsten Funktionen der Plattform, unterteilt in den Kategorien "Kollaboration/Zusammenarbeit", "Open CoDE GitLab", "Projektsteuerung", "Softwareverzeichnis" und "Wissensorganisation".

## Lizenzierungsleitfragen
Mit Hilfe unserer Lizenzierungsleitfragen kann Schritt für Schritt ermittelt werden, ob eine Open Source-Lizenz für den Softwarecode zulässig wäre. 
